# Idempiere LVE Features

- Copyright: 2021 Double Click Sistemas <https://www.dcs.com.ve>
- Repository: https://github.com/doubleclicksistemas/idempiere-lve-features
- License: GPL 2

## Description

Idempiere localization for Venezuela

## Contributors

2021 Orlando Curieles <orlando.curieles@ingeint.com>

## Components

- iDempiere Plugin [ve.com.dcs.lve-features](ve.com.dcs.lve-features)
- iDempiere Unit Test Fragment [ve.com.dcs.lve-features.test](ve.com.dcs.lve-features.test)
- iDempiere Target Platform [ve.com.dcs.lve-features.targetplatform](ve.com.dcs.lve-features.targetplatform)

## Prerequisites

- Java 11, commands `java` and `javac`.
- iDempiere 8.2.0
- Set `IDEMPIERE_REPOSITORY` env variable

## Features/Documentation

- Withholding automatizations
- Taxes and withholding
- Automatization of Currency rates


## Instructions

- Install the jar in your iDempiere environment, you must be installed the LCO before of this plugin.

## Extra Links

## Commands

Compile plugin and run tests:

```bash
./build
```

Use the parameter `debug` for debug mode example:

```bash
./build debug
```

To use `.\build.bat` for windows.
