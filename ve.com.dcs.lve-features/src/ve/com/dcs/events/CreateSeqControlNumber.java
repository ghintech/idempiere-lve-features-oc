package ve.com.dcs.events;

import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MSequence;

import ve.com.dcs.base.CustomEvent;

public class CreateSeqControlNumber extends CustomEvent {

	@Override
	protected void doHandleEvent() {
		MInvoice invoice = (MInvoice) getPO();

		if (invoice.isSOTrx()) {

			MDocType doctype = new MDocType(invoice.getCtx(), invoice.getC_DocTypeTarget_ID(), invoice.get_TrxName());

			if (doctype.get_ValueAsBoolean("IsControlNumberAuto")) {

				MSequence seq = new MSequence(invoice.getCtx(), doctype.get_ValueAsInt("ControlNumberSequence_ID"),
						invoice.get_TrxName());
				invoice.set_ValueOfColumn("ControlNumber",
						MSequence.getDocumentNoFromSeq(seq, invoice.get_TrxName(), seq));
				invoice.saveEx();
				seq.getNextID();
				seq.saveEx();
			}
		}
	}
}
