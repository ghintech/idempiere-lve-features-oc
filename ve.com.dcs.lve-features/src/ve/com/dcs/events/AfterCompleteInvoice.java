package ve.com.dcs.events;

import java.io.IOException;
import java.math.BigDecimal;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInvoice;
import org.compiere.model.MPayment;
import org.compiere.model.MSequence;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;
import org.globalqss.model.MLCOInvoiceWithholding;
import org.globalqss.model.X_LCO_WithholdingType;

import ve.com.dcs.base.CustomEvent;
import ve.com.dcs.model.MLVEWithholdingSequence;

public class AfterCompleteInvoice extends CustomEvent {

	@Override
	protected void doHandleEvent() {
		MInvoice invoice = (MInvoice) getPO();

		BigDecimal WithholdingAmt = Env.ZERO;
		try {
			WithholdingAmt = ve.com.dcs.util.Util.getWithholdingAmt(invoice);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (!invoice.isSOTrx() && WithholdingAmt.signum() > 0) {

			Integer WithholdingDocType = MSysConfig.getIntValue("WithholdingDocType", 0, invoice.getAD_Client_ID());
			Integer WithholdingBankAccount = MSysConfig.getIntValue("WithholdingBankAccount", 0,
					invoice.getAD_Client_ID());

			if (WithholdingDocType == 0)
				throw new AdempiereException("@NotCreatedWithholdingDocType@");

			if (WithholdingBankAccount == 0)
				throw new AdempiereException("@NotCreatedWithholdingAccount@");

			MPayment payment = new MPayment(invoice.getCtx(), 0, invoice.get_TrxName());
			payment.setAD_Org_ID(invoice.getAD_Org_ID());
			payment.setC_Currency_ID(invoice.getC_Currency_ID());
			payment.setC_BPartner_ID(invoice.getC_BPartner_ID());
			payment.setC_DocType_ID(WithholdingDocType);
			payment.setC_BankAccount_ID(WithholdingBankAccount);
			payment.setC_Invoice_ID(invoice.getC_Invoice_ID());
			payment.setPayAmt(Env.ZERO);
			payment.setWriteOffAmt(WithholdingAmt);
			payment.saveEx();

			String status = payment.completeIt();

			if (!status.equals(MInvoice.STATUS_Completed)) {
				throw new AdempiereException("@NotCompleted@");
			}
			payment.setDocStatus(MPayment.DOCSTATUS_Completed);
			payment.setDocAction(MPayment.STATUS_Closed);
			payment.saveEx();

			// Create Withholding Sequence
			MLCOInvoiceWithholding[] withholdings = ve.com.dcs.util.Util.getWithholdings(invoice);

			for (MLCOInvoiceWithholding wh : withholdings) {

				X_LCO_WithholdingType wht = new X_LCO_WithholdingType(invoice.getCtx(), wh.getLCO_WithholdingType_ID(),
						invoice.get_TrxName());

				if (wht.get_ValueAsBoolean("IsControlledBySequence")) {

					MLVEWithholdingSequence whseq = MLVEWithholdingSequence.getSequence(wh.getLCO_WithholdingType_ID(),
							invoice);

					String TempDocumentNo = invoice.getDateAcct().toString();
					String DocumentNo = TempDocumentNo.substring(0, 4);
					DocumentNo = DocumentNo.concat(TempDocumentNo.substring(5, 7));

					MSequence seq = new MSequence(invoice.getCtx(), whseq.getAD_Sequence_ID(), invoice.get_TrxName());
					wh.setDocumentNo(DocumentNo + MSequence.getDocumentNoFromSeq(seq, invoice.get_TrxName(), seq));
					seq.getCurrentNext();
					seq.saveEx();
					wh.saveEx();

				}

			}

		}
	}
}
