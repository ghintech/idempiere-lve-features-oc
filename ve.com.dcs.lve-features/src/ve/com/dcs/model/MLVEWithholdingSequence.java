package ve.com.dcs.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MInvoice;
import org.compiere.model.Query;
import org.globalqss.model.X_LCO_InvoiceWithholding;

public class MLVEWithholdingSequence extends X_LVE_Withholding_Sequence {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8678426979142602185L;

	public MLVEWithholdingSequence(Properties ctx, int LVE_Withholding_Sequence_ID, String trxName) {
		super(ctx, LVE_Withholding_Sequence_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MLVEWithholdingSequence(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public static MLVEWithholdingSequence getSequence(Integer WithholdingType_ID, MInvoice invoice) {

		String Where = "LCO_WithholdingType_ID = ? AND AD_Org_ID = ? ";

		MLVEWithholdingSequence seq = new Query(invoice.getCtx(), MLVEWithholdingSequence.Table_Name, Where,
				invoice.get_TrxName()).setParameters(new Object[] { WithholdingType_ID, invoice.getAD_Org_ID() })
						.first();

		return seq;

	}
	public static MLVEWithholdingSequence getSequence(X_LCO_InvoiceWithholding Withholding) {

		String Where = "LCO_WithholdingType_ID = ? AND AD_Org_ID = ? ";

		MLVEWithholdingSequence seq = new Query(Withholding.getCtx(), MLVEWithholdingSequence.Table_Name, Where,
				Withholding.get_TrxName()).setParameters(new Object[] { Withholding.getLCO_WithholdingType_ID(), Withholding.getAD_Org_ID() })
						.first();

		return seq;

	}
	

	

}
