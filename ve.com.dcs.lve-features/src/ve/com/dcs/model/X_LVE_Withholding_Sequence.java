/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package ve.com.dcs.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for LVE_Withholding_Sequence
 *  @author iDempiere (generated) 
 *  @version Release 8.2 - $Id$ */
public class X_LVE_Withholding_Sequence extends PO implements I_LVE_Withholding_Sequence, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20210502L;

    /** Standard Constructor */
    public X_LVE_Withholding_Sequence (Properties ctx, int LVE_Withholding_Sequence_ID, String trxName)
    {
      super (ctx, LVE_Withholding_Sequence_ID, trxName);
      /** if (LVE_Withholding_Sequence_ID == 0)
        {
			setLVE_Withholding_Sequence_ID (0);
        } */
    }

    /** Load Constructor */
    public X_LVE_Withholding_Sequence (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuilder sb = new StringBuilder ("X_LVE_Withholding_Sequence[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Sequence getAD_Sequence() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Sequence)MTable.get(getCtx(), org.compiere.model.I_AD_Sequence.Table_Name)
			.getPO(getAD_Sequence_ID(), get_TrxName());	}

	/** Set Sequence.
		@param AD_Sequence_ID 
		Document Sequence
	  */
	public void setAD_Sequence_ID (int AD_Sequence_ID)
	{
		if (AD_Sequence_ID < 1) 
			set_Value (COLUMNNAME_AD_Sequence_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Sequence_ID, Integer.valueOf(AD_Sequence_ID));
	}

	/** Get Sequence.
		@return Document Sequence
	  */
	public int getAD_Sequence_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Sequence_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set Withholding Type.
		@param LCO_WithholdingType_ID Withholding Type	  */
	public void setLCO_WithholdingType_ID (int LCO_WithholdingType_ID)
	{
		if (LCO_WithholdingType_ID < 1) 
			set_Value (COLUMNNAME_LCO_WithholdingType_ID, null);
		else 
			set_Value (COLUMNNAME_LCO_WithholdingType_ID, Integer.valueOf(LCO_WithholdingType_ID));
	}

	/** Get Withholding Type.
		@return Withholding Type	  */
	public int getLCO_WithholdingType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_LCO_WithholdingType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set LVE_Withholding_Sequence.
		@param LVE_Withholding_Sequence_ID LVE_Withholding_Sequence	  */
	public void setLVE_Withholding_Sequence_ID (int LVE_Withholding_Sequence_ID)
	{
		if (LVE_Withholding_Sequence_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_LVE_Withholding_Sequence_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_LVE_Withholding_Sequence_ID, Integer.valueOf(LVE_Withholding_Sequence_ID));
	}

	/** Get LVE_Withholding_Sequence.
		@return LVE_Withholding_Sequence	  */
	public int getLVE_Withholding_Sequence_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_LVE_Withholding_Sequence_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set LVE_Withholding_Sequence_UU.
		@param LVE_Withholding_Sequence_UU LVE_Withholding_Sequence_UU	  */
	public void setLVE_Withholding_Sequence_UU (String LVE_Withholding_Sequence_UU)
	{
		set_Value (COLUMNNAME_LVE_Withholding_Sequence_UU, LVE_Withholding_Sequence_UU);
	}

	/** Get LVE_Withholding_Sequence_UU.
		@return LVE_Withholding_Sequence_UU	  */
	public String getLVE_Withholding_Sequence_UU () 
	{
		return (String)get_Value(COLUMNNAME_LVE_Withholding_Sequence_UU);
	}
}